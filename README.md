User Work Flow - 

1. On Launch user can enter city name and search weather report.
2. User can add places to Favourite by tapping on heart Icon and user can see all Favourite place by tapping of "Favourite" on Toolbar.
3. User can remove the Favourite place by tapping on heart icon.
4. On Tap of each Favourite place user gets latest value of weather report.
5. User can tap on last updated text to refesh the weather data.
6. On Relaunch by default weather report of first place of Favourite list will show if internet is there app will make the api call and show latest data else app will show cached data.

API URL - http://api.openweathermap.org/data/2.5/weather?q={city}&appid={apikey}

Library used -

1. Retrofit & RxJava - For making the Api calls
2. Livadata -  To communicate between viewModel and viewModel
3. RecyclerView - To Populate Favourite list
4. GLIDE - To load the weather image.

Design Pattern - MVVM

