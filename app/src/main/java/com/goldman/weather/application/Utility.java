package com.goldman.weather.application;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;

import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class Utility {

    private Utility(){
        //nothing to do
    }

    public static final String HH_MM_A = "h:mm a";
    public static final String YYYY_MM_DD_HH_MM_A = "yyyy-MM-dd' 'h:mm a";
    private static final Double KELVIN = 273.15;
    private static final DecimalFormat df = new DecimalFormat("###.##");
    public static final String FAV_MAP_KEY = "fav_map_key";
    public static final int FAV_REQUEST_CODE = 111;
    public static final int FAV_RESULT_CODE = 222;
    public static final String FAV_NAME_INTENT_KEY = "fav_name_intent_key";
    public static final String FAV_CITY_ID_INTENT_KEY = "fav_city_id_intent_key";

    public static void hideKeyBoard(Activity activity){
        if (activity != null){
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            View view = activity.getCurrentFocus();
            if (view == null){
                view = new View(activity);
            }
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static String convertKelvinToCelsius(Double value){
        if (value != null){
            return df.format((value - KELVIN));
        }
        return "";
    }

    public static boolean isNetworkAvailable(Activity activity){
        ConnectivityManager connectivityManager = (ConnectivityManager)activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager != null && connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public static String getTimeFromMillis(BigInteger timeInUnix, String format){
        if (timeInUnix != null){
            BigInteger timeInMillis = timeInUnix.multiply(new BigInteger("1000"));
            Date date = new Date(timeInMillis.longValue());
            SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.getDefault());
            dateFormat.setTimeZone(TimeZone.getDefault());
            return dateFormat.format(date);
        }
        return "";
    }

    public static void showAlert(String message, Context context){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setMessage(message);

        builder.setPositiveButton(android.R.string.ok, null);
        builder.setNegativeButton("", null);

        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(context, android.R.color.black));
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    }
}
