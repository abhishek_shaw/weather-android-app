package com.goldman.weather.application;

import android.app.Application;

import com.goldman.weather.BuildConfig;
import com.goldman.weather.api.ApiClient;
import com.goldman.weather.api.ApiInterface;
import com.goldman.weather.api.RetrofitConfig;

import retrofit2.Retrofit;

public class WeatherApplication extends Application {

    private static WeatherApplication weatherApplication;

    public WeatherApplication(){
        //nothing to do
    }

    public static WeatherApplication getInstance(){
        return weatherApplication;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        weatherApplication = this;
    }

    public ApiInterface getWeatherRetrofitClient(){
        Retrofit retrofit = RetrofitConfig.getRetrofit(BuildConfig.WAETHER_BASE_URL);
        return retrofit.create(ApiInterface.class);
    }
}
