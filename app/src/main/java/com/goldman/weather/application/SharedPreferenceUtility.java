package com.goldman.weather.application;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferenceUtility {

    private static SharedPreferenceUtility instance;
    private static SharedPreferences sharedPreferences = null;
    private static final String SHARED_PREF_NAME = "SHARED_PREF_WEATHER_APP";

    private SharedPreferenceUtility(){
        //nothing to do
    }

    public static SharedPreferenceUtility getInstance(){
        if (instance == null){
            instance = new SharedPreferenceUtility();
        }
        return instance;
    }

    private SharedPreferences getSharedPreferences(){
        if (sharedPreferences == null){
            sharedPreferences = WeatherApplication.getInstance().getApplicationContext().getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        }
        return sharedPreferences;
    }

    public String getStringFromSharedPref(String key){
        String defaultValue = "";
        try {
            if (sharedPreferences == null){
                sharedPreferences = getSharedPreferences();
            }
            return sharedPreferences.getString(key, defaultValue);
        } catch (Exception e){
            e.printStackTrace();
        }
        return defaultValue;
    }

    public void addStringToSharedPref(String key, String value){
        try {
            if (sharedPreferences == null){
                sharedPreferences = getSharedPreferences();
            }
            sharedPreferences.edit().putString(key, value).apply();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

}
