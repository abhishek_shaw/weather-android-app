package com.goldman.weather.module.searchweather.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.goldman.weather.BuildConfig;
import com.goldman.weather.application.SharedPreferenceUtility;
import com.goldman.weather.application.Utility;
import com.goldman.weather.application.WeatherApplication;
import com.goldman.weather.model.weather.WeatherResponse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.internal.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchWeatherViewModel extends ViewModel {

    public MutableLiveData<Boolean> progressbar = new MutableLiveData<>();
    public MutableLiveData<WeatherResponse> weatherResponseMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<String> weatherErrorMutableLiveData = new MutableLiveData<>();

    public void searchWeatherForCity(String cityName) {
        progressbar.setValue(true);
        Call<WeatherResponse> weatherForCity = WeatherApplication.getInstance()
                .getWeatherRetrofitClient().getWeatherForCity(cityName, BuildConfig.APIKEY);

        weatherForCity.enqueue(new Callback<WeatherResponse>() {
            @Override
            public void onResponse(@NotNull Call<WeatherResponse> call, @NotNull Response<WeatherResponse> response) {
                progressbar.setValue(false);
                if (response.isSuccessful() && response.body() != null){
                    weatherResponseMutableLiveData.setValue(response.body());
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        if (jsonObject.has("message")){
                            weatherErrorMutableLiveData.setValue(jsonObject.getString("message"));
                        } else {
                            weatherErrorMutableLiveData.setValue("");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        weatherErrorMutableLiveData.setValue("");
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<WeatherResponse> call, @NotNull Throwable t) {
                progressbar.setValue(false);
                weatherErrorMutableLiveData.setValue("");
            }
        });
    }

    public Map<Integer, WeatherResponse> getFavMapFromSharedPref() {
        String favMapString = SharedPreferenceUtility.getInstance().getStringFromSharedPref(Utility.FAV_MAP_KEY);
        return new Gson().fromJson( favMapString, new TypeToken<HashMap<Integer, WeatherResponse>>(){}.getType());
    }

    public void addPlaceToFavMap(int id , WeatherResponse weatherResponse){

        Map<Integer, WeatherResponse> favMap = getFavMapFromSharedPref();
        if (favMap == null){
            favMap = new HashMap<>();
        }
        favMap.put(id, weatherResponse);

        Gson gson = new Gson();
        String mapString = gson.toJson(favMap);

        SharedPreferenceUtility.getInstance().addStringToSharedPref(Utility.FAV_MAP_KEY, mapString);
    }

    public void removePlaceFromFavMap(int id){

        Map<Integer, WeatherResponse> favMap = getFavMapFromSharedPref();
        if (favMap == null){
            favMap = new HashMap<>();
        }
        favMap.remove(id);

        Gson gson = new Gson();
        String mapString = gson.toJson(favMap);

        SharedPreferenceUtility.getInstance().addStringToSharedPref(Utility.FAV_MAP_KEY, mapString);
    }
}