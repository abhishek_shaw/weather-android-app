package com.goldman.weather.module.searchweather.view;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.goldman.weather.BuildConfig;
import com.goldman.weather.R;
import com.goldman.weather.application.Utility;
import com.goldman.weather.model.weather.WeatherResponse;
import com.goldman.weather.module.searchweather.router.SearchWeatherRouter;
import com.goldman.weather.module.searchweather.viewmodel.SearchWeatherViewModel;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Map;

public class SearchWeatherFragment extends Fragment {

    private SearchWeatherViewModel mViewModel;
    private TextInputEditText etCity;
    private ScrollView scrollLayout;
    private TextView tvPlace;
    private TextView tvWeather;
    private TextView tvCurrentTemp;
    private TextView tvHumidity;
    private TextView tvWindSpeed;
    private TextView tvSunrise;
    private TextView tvSunset;
    private TextView tvLastUpdate;
    private ImageView imgFav;
    private ImageView imgWeather;
    private static WeatherResponse weatherResponse;
    public Map<Integer, WeatherResponse> favPlaceMap;

    private static final Integer FAV_RESULT_CODE = 111;

    public static SearchWeatherFragment newInstance() {
        return new SearchWeatherFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_weather_fragment, container, false);
        initView(view);
        setHasOptionsMenu(true);
        return view;
    }

    private void initView(View view) {
        scrollLayout = view.findViewById(R.id.scroll_layout);
        tvPlace = view.findViewById(R.id.tv_place);
        tvWeather = view.findViewById(R.id.tv_weather);
        tvCurrentTemp = view.findViewById(R.id.tv_current_temp);
        tvHumidity = view.findViewById(R.id.tv_humidity);
        tvWindSpeed = view.findViewById(R.id.tv_wind_speed);
        tvSunrise = view.findViewById(R.id.tv_sunrise);
        tvSunset = view.findViewById(R.id.tv_sunset);
        tvLastUpdate = view.findViewById(R.id.tv_last_update);
        ImageView imageSearch = view.findViewById(R.id.img_search);
        etCity = view.findViewById(R.id.et_city);
        imgFav = view.findViewById(R.id.img_fav);
        imgWeather = view.findViewById(R.id.img_weather);
        scrollLayout.setVisibility(View.GONE);
        imageSearch.setOnClickListener(view1 -> {
            handleOnSearch();
        });

        etCity.setOnEditorActionListener((textView, actionId, keyEvent) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_SEARCH) {
                handleOnSearch();
                return true;
            }
            return false;
        });

        tvLastUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getActivity() != null && mViewModel != null) {
                    if (Utility.isNetworkAvailable(getActivity())) {
                        if (!TextUtils.isEmpty(weatherResponse.getName())) {
                            mViewModel.searchWeatherForCity(weatherResponse.getName());
                        }
                    } else {
                        Utility.showAlert(getString(R.string.no_internet), getActivity());
                    }
                }
            }
        });

        imgFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getActivity() == null) return;
                if (favPlaceMap != null && favPlaceMap.containsKey(weatherResponse.getId())){
                    imgFav.setImageResource(R.drawable.ic_unfavorite);
                    mViewModel.removePlaceFromFavMap(weatherResponse.getId());
                    Toast.makeText(getActivity(), getString(R.string.fav_removed_text), Toast.LENGTH_SHORT).show();
                } else {
                    mViewModel.addPlaceToFavMap(weatherResponse.getId(), weatherResponse);
                    imgFav.setImageResource(R.drawable.ic_favorite);
                    Toast.makeText(getActivity(), getString(R.string.fav_added_text), Toast.LENGTH_SHORT).show();
                }
                favPlaceMap = mViewModel.getFavMapFromSharedPref();
            }
        });
    }

    private void handleOnSearch() {
        if (getActivity() == null || mViewModel == null) return;
        if (Utility.isNetworkAvailable(getActivity())) {
            if (etCity.getText() != null && etCity.getText().length() > 0) {
                mViewModel.searchWeatherForCity(etCity.getText().toString());
            }
        } else {
            Utility.showAlert(getString(R.string.no_internet), getActivity());
        }
        Utility.hideKeyBoard(getActivity());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(SearchWeatherViewModel.class);

        favPlaceMap = mViewModel.getFavMapFromSharedPref();

        if (getActivity() != null && favPlaceMap != null && !favPlaceMap.isEmpty()){
            WeatherResponse weatherResponse = favPlaceMap.entrySet().iterator().next().getValue();
            if (Utility.isNetworkAvailable(getActivity())){
                mViewModel.searchWeatherForCity(weatherResponse.getName());
            } else {
                scrollLayout.setVisibility(View.VISIBLE);
                updateUI(weatherResponse);
            }
        }

        mViewModel.progressbar.observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean showProgress) {
                if (getActivity() != null) {
                    ((SearchWeatherActivity) getActivity()).progressBarVisibility(showProgress);
                }
            }
        });

        mViewModel.weatherResponseMutableLiveData.observe(getViewLifecycleOwner(), new Observer<WeatherResponse>() {
            @Override
            public void onChanged(WeatherResponse weatherResponse) {
                scrollLayout.setVisibility(View.VISIBLE);
                updateUI(weatherResponse);
            }
        });

        mViewModel.weatherErrorMutableLiveData.observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(String errorMessage) {
                if (getActivity() == null) return;

                if (!TextUtils.isEmpty(errorMessage)) {
                    Utility.showAlert(errorMessage, getActivity());
                } else {
                    Utility.showAlert(getString(R.string.something_went_wrong), getActivity());
                }
            }
        });
    }

    private void updateUI(WeatherResponse weatherResponse) {
        SearchWeatherFragment.weatherResponse = weatherResponse;
        if (getActivity() != null) {
            if (favPlaceMap != null && favPlaceMap.containsKey(weatherResponse.getId())){
                imgFav.setImageResource(R.drawable.ic_favorite);
            } else {
                imgFav.setImageResource(R.drawable.ic_unfavorite);
            }
            tvPlace.setText(weatherResponse.getName());
            if (weatherResponse.getWeather() != null && !weatherResponse.getWeather().isEmpty()) {
                tvWeather.setText(getString(R.string.weather_desc,
                        weatherResponse.getWeather().get(0).getMain(), weatherResponse.getWeather().get(0).getDescription()));
                String imageUrl = BuildConfig.IMAGE_BASE_URL + weatherResponse.getWeather().get(0).getIcon() + ".png";
                Glide.with(this).load(imageUrl).centerCrop().into(imgWeather);
            }
            if (weatherResponse.getMain() != null) {
                String currentTemp = Utility.convertKelvinToCelsius(weatherResponse.getMain().getTemp());
                String feelsLike = Utility.convertKelvinToCelsius(weatherResponse.getMain().getFeelsLike());
                tvCurrentTemp.setText(getString(R.string.current_temp, currentTemp, feelsLike));
                tvHumidity.setText(getString(R.string.humidity, weatherResponse.getMain().getHumidity() + "%"));
            }
            tvWindSpeed.setText(getString(R.string.wind_speed, String.valueOf(weatherResponse.getWind().getSpeed())));
            String sunriseTime = Utility.getTimeFromMillis(weatherResponse.getSys().getSunrise(), Utility.HH_MM_A);
            String sunsetTime = Utility.getTimeFromMillis(weatherResponse.getSys().getSunset(), Utility.HH_MM_A);
            tvSunrise.setText(getString(R.string.sunrise, sunriseTime));
            tvSunset.setText(getString(R.string.sunset, sunsetTime));
            String lastUpdateTime = Utility.getTimeFromMillis(weatherResponse.getDt(), Utility.YYYY_MM_DD_HH_MM_A);
            tvLastUpdate.setText(getString(R.string.last_updated_date_time, lastUpdateTime));
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.fav_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.fav) {
            if (getActivity() != null) {
                SearchWeatherRouter.launchFavouriteListScreen(getActivity(), Utility.FAV_REQUEST_CODE);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Utility.FAV_REQUEST_CODE && resultCode == Utility.FAV_RESULT_CODE && data != null) {
            String placeName = data.getStringExtra(Utility.FAV_NAME_INTENT_KEY);
            int cityId = data.getIntExtra(Utility.FAV_CITY_ID_INTENT_KEY, 0);
            if (getActivity() == null) return;

            if (Utility.isNetworkAvailable(getActivity())) {
                mViewModel.searchWeatherForCity(placeName);
            } else if (favPlaceMap != null && !favPlaceMap.isEmpty()) {
                WeatherResponse weatherResponse = favPlaceMap.get(cityId);
                if (weatherResponse != null) {
                    scrollLayout.setVisibility(View.VISIBLE);
                    updateUI(weatherResponse);
                }
            }
        }
    }
}