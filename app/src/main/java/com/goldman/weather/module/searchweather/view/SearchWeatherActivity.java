package com.goldman.weather.module.searchweather.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.goldman.weather.R;

public class SearchWeatherActivity extends AppCompatActivity {

    private RelativeLayout progressLayout;
    private SearchWeatherFragment searchWeatherFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_weather);
        progressLayout = findViewById(R.id.progress_layout);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setTitle(getString(R.string.weather));
        }
        addSearchWeatherFragment();
    }

    private void addSearchWeatherFragment() {
        searchWeatherFragment = SearchWeatherFragment.newInstance();
        getSupportFragmentManager().beginTransaction().add(R.id.frame_container, searchWeatherFragment).commit();
    }

    public void progressBarVisibility(boolean showProgress){

        if (showProgress){
            progressLayout.setVisibility(View.VISIBLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            progressLayout.setVisibility(View.GONE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (searchWeatherFragment != null){
            searchWeatherFragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}