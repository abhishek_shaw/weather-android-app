package com.goldman.weather.module.favourite.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.goldman.weather.application.SharedPreferenceUtility;
import com.goldman.weather.application.Utility;
import com.goldman.weather.model.weather.WeatherResponse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FavouriteViewModel extends ViewModel {

    public MutableLiveData<List<WeatherResponse>> favListLiveData = new MutableLiveData<>();

    public void getFavPlaceList() {
        String favMapString = SharedPreferenceUtility.getInstance().getStringFromSharedPref(Utility.FAV_MAP_KEY);
        Map<Integer, WeatherResponse> favMap = new Gson().fromJson(favMapString, new TypeToken<HashMap<Integer, WeatherResponse>>() {
        }.getType());

        if (favMap != null){
            favListLiveData.setValue(new ArrayList<>(favMap.values()));
        } else {
            favListLiveData.setValue(null);
        }
    }
}
