package com.goldman.weather.module.searchweather.router;

import android.app.Activity;
import android.content.Intent;

import com.goldman.weather.module.favourite.view.FavouriteActivity;

public class SearchWeatherRouter {

    private SearchWeatherRouter(){
        //nothing to do
    }

    public static void launchFavouriteListScreen(Activity activity, int requestCode){
        if (activity != null){
            Intent intent = new Intent(activity, FavouriteActivity.class);
            activity.startActivityForResult(intent, requestCode);
        }
    }
}
