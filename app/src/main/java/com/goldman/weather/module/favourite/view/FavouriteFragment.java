package com.goldman.weather.module.favourite.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.goldman.weather.R;
import com.goldman.weather.application.Utility;
import com.goldman.weather.model.weather.WeatherResponse;
import com.goldman.weather.module.favourite.adapter.FavouriteAdapter;
import com.goldman.weather.module.favourite.viewmodel.FavouriteViewModel;

import java.util.List;

public class FavouriteFragment extends Fragment implements FavouriteAdapter.IFavClickListener {

    private TextView tvNoFavPlace;
    private RecyclerView rvFavPlace;

    public static FavouriteFragment newInstance(){
        return new FavouriteFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favourite, container, false);
        setHasOptionsMenu(true);
        initView(view);
        return view;
    }

    private void initView(View view) {
        tvNoFavPlace = view.findViewById(R.id.tv_no_fav_place);
        rvFavPlace = view.findViewById(R.id.rv_fav_place);
        rvFavPlace.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FavouriteViewModel mViewModel = new ViewModelProvider(this).get(FavouriteViewModel.class);
        mViewModel.getFavPlaceList();

        mViewModel.favListLiveData.observe(getViewLifecycleOwner(), new Observer<List<WeatherResponse>>() {
            @Override
            public void onChanged(List<WeatherResponse> favList) {
                if (favList != null && !favList.isEmpty()){
                    rvFavPlace.setVisibility(View.VISIBLE);
                    tvNoFavPlace.setVisibility(View.GONE);
                    FavouriteAdapter favouriteAdapter = new FavouriteAdapter(favList, FavouriteFragment.this);
                    rvFavPlace.setAdapter(favouriteAdapter);
                } else {
                    rvFavPlace.setVisibility(View.GONE);
                    tvNoFavPlace.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public void onFavItemClick(WeatherResponse weatherResponse) {
        if (getActivity() != null) {
            Intent intent = new Intent();
            intent.putExtra(Utility.FAV_NAME_INTENT_KEY, weatherResponse.getName());
            intent.putExtra(Utility.FAV_CITY_ID_INTENT_KEY, weatherResponse.getId());
            getActivity().setResult(Utility.FAV_RESULT_CODE, intent);
            getActivity().finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            if (getActivity() != null){
                getActivity().onBackPressed();
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
