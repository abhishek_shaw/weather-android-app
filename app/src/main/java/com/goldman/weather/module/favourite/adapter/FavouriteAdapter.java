package com.goldman.weather.module.favourite.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.goldman.weather.R;
import com.goldman.weather.model.weather.WeatherResponse;

import java.util.List;

public class FavouriteAdapter extends RecyclerView.Adapter<FavouriteAdapter.ItemViewHolder> {

    private final List<WeatherResponse> favPlaceList;
    private final IFavClickListener clickListener;

    public FavouriteAdapter(List<WeatherResponse> favPlaceList, IFavClickListener favClickListener){
        this.favPlaceList = favPlaceList;
        this.clickListener = favClickListener;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_fav_place, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        holder.tvFavName.setText(favPlaceList.get(position).getName());
        holder.tvFavName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickListener.onFavItemClick(favPlaceList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return favPlaceList.size();
    }


    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        private TextView tvFavName;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tvFavName = itemView.findViewById(R.id.tv_fav_name);
        }
    }

    public interface IFavClickListener{
        void onFavItemClick(WeatherResponse weatherResponse);
    }

}
