package com.goldman.weather.api;

import com.goldman.weather.model.weather.WeatherResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("data/2.5/weather")
    Call<WeatherResponse> getWeatherForCity(@Query("q") String cityName, @Query("appid") String apikey);
}
